import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BisectionComponent } from './components/bisection/bisection.component';
import { FalsePositionComponent } from './components/false-position/false-position.component';
import { OnePointComponent } from './components/one-point/one-point.component';

const routes: Routes = [
  { path: 'bisection', component: BisectionComponent },
  { path: 'falseposition', component: FalsePositionComponent },
  { path: 'onepoint', component: OnePointComponent },
  { path: 'newton', component: OnePointComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
