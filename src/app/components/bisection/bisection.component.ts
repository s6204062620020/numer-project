import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { bisectionModel } from './bisectionModel';

@Component({
  selector: 'app-bisection',
  templateUrl: './bisection.component.html',
  styleUrls: ['./bisection.component.css']
})
export class BisectionComponent implements OnInit {

  bisection !: bisectionModel[]
  bisectionForm !: FormGroup

  Result: Number


  constructor(private form: FormBuilder) {
    this.bisectionForm = this.form.group({
      fx: [''],
      xL: [0],
      xR: [0]
    })

    this.Result = 0
  }

  ngOnInit(): void { }

  onSubmit(bisectionValue:bisectionModel){
    let take = (x: number) => ( Math.pow(13, 1/4)-x)

    let mid = (L: number, R: number) => ((L + R) / 2.0)

    let cross = (fxM: number, fxR: number) => (fxM * fxR)

    let abs = (NEW: number, OLD: number) => (Math.abs((NEW - OLD) / NEW))
    let res= 1, xold = 0, XM = 0;
    let e = 0.000001;

    do{
      XM = mid(bisectionValue.xL,bisectionValue.xR);
      let fxr = take(bisectionValue.xR);
      let fxm = take(bisectionValue.xR);
      if(cross(fxm,fxr)>0){
        if(cross(fxm,fxr)>0){
          xold = bisectionValue.xR;
          bisectionValue.xR = XM;
        }
        else{
          xold = bisectionValue.xL;
          bisectionValue.xL = XM;
        }
        res = abs(XM,xold);
      }
    }
    while(res>e){
      return this.Result = XM;
    }
  }

}
