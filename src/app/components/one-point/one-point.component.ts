import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { OnepointModel } from './onepoint-model';

@Component({
  selector: 'app-one-point',
  templateUrl: './one-point.component.html',
  styleUrls: ['./one-point.component.css']
})
export class OnePointComponent implements OnInit {

  onePoint !: OnepointModel[]
  onePointForm !: FormGroup

  Result: Number


    constructor(private form: FormBuilder) {
    this.onePointForm = this.form.group({
      fx: [''],
      x: [0]

    })

    this.Result = 0
  }


  ngOnInit(): void {
  }


onSubmit(onePointValue: OnepointModel){
  let getFx = (x: number) => {return Math.cos(2 * x * Math.PI/3)} ;

  let x_new = onePointValue.x ;
  let x_old = 0 ;
  let err = 0 ;
  do{
      x_old = x_new ;
      x_new = getFx(x_old) ;
      console.log("X = " + x_new.toFixed(6)) ;
      err = Math.abs((x_new - x_old) / x_new ) ;
  }
  while(err > 0.000001) ;
  //console.log("X = " + x_new.toFixed(6)) ;
  return this.Result = x_new ;
}
}
