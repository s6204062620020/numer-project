import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { falseModel } from './falseModel';

@Component({
  selector: 'app-false-position',
  templateUrl: './false-position.component.html',
  styleUrls: ['./false-position.component.css']
})
export class FalsePositionComponent implements OnInit {

  false !: falseModel[]
  falseForm !: FormGroup

  Result: Number


  constructor(private form: FormBuilder) {
    this.falseForm = this.form.group({
      fx: [''],
      xL: [0],
      xR: [0]
    })

    this.Result = 0
  }

  ngOnInit(): void { }

  onSubmit(value: falseModel){
    let getFx = (x:number) => {return (43*x -1)} ;

    let getFP = (l:number,r:number) => {
        let fxl = getFx(l) ;
        let fxr = getFx(r) ;

        let x1 = (l*fxr - r*fxl) / (fxr - fxl) ;
        let fx1 = getFx(x1) ;

        if(fx1 * fxr < 0){
            l = x1 ;
        }
        else{
            r = x1 ;
        }
        return{l , x1 , r } ;
    }

    let l = value.xL ;
    let r = value.xR ;
    let x_old = 0 ;
    let x_new = 0 ;
    let err = 0;

    do{
        x_old = x_new ;
        let fp = getFP(l,r) ;
        x_new = fp.x1 ;

        err = Math.abs((x_new - x_old) / x_new ) ;
        l = fp.l ;
        r = fp.r ;
    }
    while(err > 0.000001);
    //console.log("X = " + x_new.toFixed(6));
    return this.Result = x_new;
  }

}
