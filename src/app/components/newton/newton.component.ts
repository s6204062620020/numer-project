import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NewtonModel } from './newtonModel';

@Component({
  selector: 'app-newton',
  templateUrl: './newton.component.html',
  styleUrls: ['./newton.component.css']
})
export class NewtonComponent implements OnInit {

  Newton !: NewtonModel[]
  NewtonForm !: FormGroup

  Result: Number


  constructor(private form: FormBuilder) {
    this.NewtonForm = this.form.group({
      fx: [''],
      x: [0]
    })

    this.Result = 0
  }

  ngOnInit(): void { }

  onSubmit(NewtonValue: NewtonModel) {

    let calfx = (x: number) => Math.pow(7, 1 / 2) - x;

    let calabs = (mnew : number, mold : number) => Math.abs((mnew - mold) / mnew);

    let x = NewtonValue.x;
    let xn = 0;
    let fx = 0, abs = 0;
    while (true) {
      fx = calfx(x);
      xn = x - (fx / -1);
      calabs(xn, x);
      x = xn;
      if (-0.000001 <= abs && abs <= 0.000001) {
        // console.log("xn = " + xn.toFixed(6))
        break;
      }
    }
    return this.Result = xn ;
  }
}
