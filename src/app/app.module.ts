import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BisectionComponent } from './components/bisection/bisection.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { FalsePositionComponent } from './components/false-position/false-position.component';
import { OnePointComponent } from './components/one-point/one-point.component';
import { NewtonComponent } from './components/newton/newton.component';

@NgModule({
  declarations: [
    AppComponent,
    BisectionComponent,
    TopbarComponent,
    FalsePositionComponent,
    OnePointComponent,
    NewtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
